db.users.find({
    $or: [
        {
            firstName: {
                $regex: 's',
                $options: '$i'
            }
        },
        {
            lastName: {
                $regex: 'd',
                $options: '$i'
            }
        }
    ]
})

db.users.find({
    $and: [
        {
            department: {
                $in: ["HR"]
            }
        },
        {
            age: {
                $gte: 70
            }
        }
    ]
})

db.users.find({
    $and: [
        {
            firstName: {
                $regex: 'e',
                $options: '$i'
            }
        },
        {
            age: {
                $lte: 30
            }
        }
    ]
})
